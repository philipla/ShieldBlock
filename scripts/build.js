const fs = require('fs');
const browserify = require('browserify');

const start = Date.now();

const background = browserify('./src/background.js')
  .transform('babelify', {presets: ['babel-preset-env']})
  .bundle()
  .pipe(fs.createWriteStream('background/background.js'));

const popup = browserify('./src/popup.js')
  .transform('babelify', {
    presets: ['babel-preset-env'],
    plugins: [['transform-react-jsx', { 'pragma': 'h' }]]
  })
  .bundle()
  .pipe(fs.createWriteStream('popup/popup.js'));

function timeDiff() {
  return (Date.now() - start) + " ms";
}

background.on('finish', () => console.log(timeDiff(), 'Background finished'));
popup.on('finish', () => console.log(timeDiff(), 'Popup finished'));
