#!/bin/bash

FILE=src/popup.js

echo "Commenting out $FILE"

echo -e "//NOTE: JSX fails the addon checker\n/*\n$(cat $FILE)" > $FILE
echo "*/" >> $FILE

zip -r -FS shieldblock.zip * -x "*.git*" "*node_modules*"

sed -i '1,2d' $FILE
sed -i '$d' $FILE

echo "Done creating zip!"
