(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
!function(e,n){"object"==typeof exports&&"undefined"!=typeof module?n(exports):"function"==typeof define&&define.amd?define(["exports"],n):n(e.hyperapp={})}(this,function(e){"use strict";e.h=function(e,n){for(var t=[],r=[],o=arguments.length;2<o--;)t.push(arguments[o]);for(;t.length;){var u=t.pop();if(u&&u.pop)for(o=u.length;o--;)t.push(u[o]);else null!=u&&!0!==u&&!1!==u&&r.push(u)}return"function"==typeof e?e(n||{},r):{nodeName:e,attributes:n||{},children:r,key:n&&n.key}},e.app=function(e,n,t,r){var o,u=[].map,l=r&&r.children[0]||null,i=l&&function n(e){return{nodeName:e.nodeName.toLowerCase(),attributes:{},children:u.call(e.childNodes,function(e){return 3===e.nodeType?e.nodeValue:n(e)})}}(l),y=[],g=!0,f=b(e),a=function e(r,o,u){for(var n in u)"function"==typeof u[n]?function(e,t){u[e]=function(e){var n=t(e);return"function"==typeof n&&(n=n(v(r,f),u)),n&&n!==(o=v(r,f))&&!n.then&&s(f=d(r,b(o,n),f)),n}}(n,u[n]):e(r.concat(n),o[n]=b(o[n]),u[n]=b(u[n]));return u}([],f,b(n));return s(),a;function N(e){return"function"==typeof e?N(e(f,a)):null!=e?e:""}function c(){o=!o;var e=N(t);for(r&&!o&&(l=function e(n,t,r,o,u){if(o===r);else if(null==r||r.nodeName!==o.nodeName){var l=function e(n,t){var r="string"==typeof n||"number"==typeof n?document.createTextNode(n):(t=t||"svg"===n.nodeName)?document.createElementNS("http://www.w3.org/2000/svg",n.nodeName):document.createElement(n.nodeName),o=n.attributes;if(o){o.oncreate&&y.push(function(){o.oncreate(r)});for(var u=0;u<n.children.length;u++)r.appendChild(e(n.children[u]=N(n.children[u]),t));for(var l in o)w(r,l,o[l],null,t)}return r}(o,u);n.insertBefore(l,t),null!=r&&x(n,t,r),t=l}else if(null==r.nodeName)t.nodeValue=o;else{!function(e,n,t,r){for(var o in b(n,t))t[o]!==("value"===o||"checked"===o?e[o]:n[o])&&w(e,o,t[o],n[o],r);var u=g?t.oncreate:t.onupdate;u&&y.push(function(){u(e,n)})}(t,r.attributes,o.attributes,u=u||"svg"===o.nodeName);for(var i={},f={},a=[],c=r.children,s=o.children,d=0;d<c.length;d++){a[d]=t.childNodes[d];var v=k(c[d]);null!=v&&(i[v]=[a[d],c[d]])}for(var d=0,h=0;h<s.length;){var v=k(c[d]),p=k(s[h]=N(s[h]));if(f[v])d++;else if(null==p||p!==k(c[d+1]))if(null==p||g)null==v&&(e(t,a[d],c[d],s[h],u),h++),d++;else{var m=i[p]||[];v===p?(e(t,m[0],m[1],s[h],u),d++):m[0]?e(t,t.insertBefore(m[0],a[d]),m[1],s[h],u):e(t,a[d],null,s[h],u),f[p]=s[h],h++}else null==v&&x(t,a[d],c[d]),d++}for(;d<c.length;)null==k(c[d])&&x(t,a[d],c[d]),d++;for(var d in i)f[d]||x(t,i[d][0],i[d][1])}return t}(r,l,i,i=e)),g=!1;y.length;)y.pop()()}function s(){o||(o=!0,setTimeout(c))}function b(e,n){var t={};for(var r in e)t[r]=e[r];for(var r in n)t[r]=n[r];return t}function d(e,n,t){var r={};return e.length?(r[e[0]]=1<e.length?d(e.slice(1),n,t[e[0]]):n,b(t,r)):n}function v(e,n){for(var t=0;t<e.length;)n=n[e[t++]];return n}function k(e){return e?e.key:null}function h(e){return e.currentTarget.events[e.type](e)}function w(e,n,t,r,o){if("key"===n);else if("style"===n)for(var u in b(r,t)){var l=null==t||null==t[u]?"":t[u];"-"===u[0]?e[n].setProperty(u,l):e[n][u]=l}else"o"===n[0]&&"n"===n[1]?(n=n.slice(2),e.events?r||(r=e.events[n]):e.events={},(e.events[n]=t)?r||e.addEventListener(n,h):e.removeEventListener(n,h)):n in e&&"list"!==n&&"type"!==n&&"draggable"!==n&&"spellcheck"!==n&&"translate"!==n&&!o?e[n]=null==t?"":t:null!=t&&!1!==t&&e.setAttribute(n,t),null!=t&&!1!==t||e.removeAttribute(n)}function x(e,n,t){function r(){e.removeChild(function e(n,t){var r=t.attributes;if(r){for(var o=0;o<t.children.length;o++)e(n.childNodes[o],t.children[o]);r.ondestroy&&r.ondestroy(n)}return n}(n,t))}var o=t.attributes&&t.attributes.onremove;o?o(n,r):r()}}});

},{}],2:[function(require,module,exports){
(function (global){
/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as the `TypeError` message for "Functions" methods. */
var FUNC_ERROR_TEXT = 'Expected a function';

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        result = wait - timeSinceLastCall;

    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && objectToString.call(value) == symbolTag);
}

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = debounce;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],3:[function(require,module,exports){
'use strict';

function parseFormat(str) {
  var list = [];
  var spaces = str.split(' ');
  if (spaces.length < 1) return null;
  list[0] = spaces[0];
  if (spaces.length > 1) {
    if (spaces[1].indexOf('except:') >= 0) {
      var commas = spaces[1].split(':')[1].split(',');
      if (commas.length < 1) return null;
      return list.concat(commas);
    } else {
      return null;
    }
  } else {
    return list;
  }
}

module.exports = parseFormat;

},{}],4:[function(require,module,exports){
'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _hyperapp = require('hyperapp');

var _lodash = require('lodash.debounce');

var _lodash2 = _interopRequireDefault(_lodash);

var _parseformat = require('./parseformat');

var _parseformat2 = _interopRequireDefault(_parseformat);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function saveState(todos) {
  var hostNames = {};
  for (var i = 0; i < todos.length; i++) {
    var todo = todos[i];
    if (!todo.unchecked) {
      var result = (0, _parseformat2.default)(todo.value);
      if (result) {
        hostNames[result[0]] = result.slice(1);
      }
    }
  }

  browser.runtime.sendMessage({ type: 'hostnames', value: hostNames });

  browser.storage.local.set({ shieldblockhostnames: hostNames });
  browser.storage.local.set({ shieldblocktodos: todos });
}

var state = {
  todos: [],
  input: "",
  error: "",
  redirect: "https://nghttp2.org/httpbin/robots.txt"
};

var actions = {
  add: function add() {
    return function (state) {
      if (state.input == '') return;

      var result = (0, _parseformat2.default)(state.input);
      var todos = state.todos.concat({ value: state.input, unchecked: false });

      if (result) {
        saveState(todos);
        return {
          input: "",
          error: "",
          todos: todos
        };
      } else {
        return { error: "invalid format" };
      }
    };
  },
  todoinput: function todoinput(_ref) {
    var value = _ref.value;
    return { input: value };
  },
  redirectinput: function redirectinput(_ref2) {
    var value = _ref2.value;

    browser.runtime.sendMessage({ type: 'redirect', value: value });
    browser.storage.local.set({ shieldblockredirect: value });

    return { redirect: value };
  },
  remove: function remove(_ref3) {
    var todos = _ref3.todos,
        index = _ref3.index;

    todos.splice(index, 1);
    saveState(todos);

    return { todos: todos };
  },
  toggle: function toggle(_ref4) {
    var todos = _ref4.todos,
        index = _ref4.index;

    var todo = todos[index];
    todo.unchecked = !todo.unchecked;
    saveState(todos);
    return { todos: todos };
  }
};

var view = function view(state, actions) {
  return (0, _hyperapp.h)(
    'div',
    null,
    (0, _hyperapp.h)(
      'h2',
      null,
      'ShieldBlock'
    ),
    (0, _hyperapp.h)(
      'div',
      null,
      (0, _hyperapp.h)(
        'label',
        null,
        'Redirect Url:'
      ),
      (0, _hyperapp.h)('input', {
        id: 'redirect-input',
        type: 'text',
        oninput: (0, _lodash2.default)(function (e) {
          return actions.redirectinput({ value: e.target.value });
        }, 250),
        value: state.redirect
      })
    ),
    (0, _hyperapp.h)(
      'div',
      null,
      (0, _hyperapp.h)('input', {
        id: 'todo-input',
        type: 'text',
        onkeyup: function onkeyup(e) {
          return e.keyCode === 13 ? actions.add() : "";
        },
        oninput: function oninput(e) {
          return actions.todoinput({ value: e.target.value });
        },
        value: state.input,
        placeholder: 'format example: www.website.com except:/page1,/page2'
      }),
      (0, _hyperapp.h)(
        'button',
        { onclick: actions.add },
        '\uFF0B'
      )
    ),
    (0, _hyperapp.h)(
      'p',
      null,
      (0, _hyperapp.h)(
        'h2',
        null,
        state.error
      ),
      (0, _hyperapp.h)(
        'ul',
        null,
        state.todos.map(function (t, index) {
          return (0, _hyperapp.h)(
            'li',
            null,
            (0, _hyperapp.h)('input', { type: 'checkbox', checked: !t.unchecked,
              onchange: function onchange(e) {
                return actions.toggle({ todos: state.todos, index: index });
              } }),
            t.value,
            (0, _hyperapp.h)(
              'button',
              {
                onclick: function onclick() {
                  return actions.remove({ todos: state.todos, index: index });
                } },
              'x'
            )
          );
        })
      )
    )
  );
};

Promise.all([browser.storage.local.get('shieldblocktodos'), browser.storage.local.get('shieldblockredirect'), browser.storage.local.get('shieldblockcurrentsite')]).then(function (result) {
  var _result = _slicedToArray(result, 3),
      todos = _result[0],
      redirect = _result[1],
      currentsite = _result[2];

  if (Array.isArray(todos.shieldblocktodos)) {
    state.todos = todos.shieldblocktodos;
  }

  if (redirect.shieldblockredirect) {
    state.redirect = redirect.shieldblockredirect;
  }

  console.log(14, currentsite.shieldblockcurrentsite);
  if (currentsite.shieldblockcurrentsite) {
    state.input = currentsite.shieldblockcurrentsite;
  }

  (0, _hyperapp.app)(state, actions, view, document.getElementById('app'));
});

},{"./parseformat":3,"hyperapp":1,"lodash.debounce":2}]},{},[4]);
