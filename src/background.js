import URL from 'url-parse';

const DEFAULT_REDIRECT_URL = 'https://nghttp2.org/httpbin/robots.txt';

const FILTER = {
  urls: ["<all_urls>"],
  types: ["main_frame"]
};

let hostNames = {};
let redirectUrl = DEFAULT_REDIRECT_URL;

browser.storage.local.get('shieldblockhostnames')
  .then(({ shieldblockhostnames }) => hostNames = shieldblockhostnames);

browser.storage.local.get('shieldblockredirect')
  .then(({ shieldblockredirect }) => redirectUrl = shieldblockredirect);

function onMessage(message) {
  switch (message.type) {
    case 'hostnames':
      hostNames = message.value;
      break;
    case 'redirect':
      redirectUrl = message.value;
      break;
    default:
      console.error('Unknown message: ', message);
  }
}

function redirect(req) {
  if (!hostNames) hostNames = {};
  if (!redirectUrl) redirectUrl = DEFAULT_REDIRECT_URL;

  console.log(3, req.url, req);

  let url = new URL(req.url);
  console.log(9, hostNames, url.hostname, url.pathname, redirectUrl);

  browser.storage.local.set({ shieldblockcurrentsite: url.hostname });

  if (hostNames.hasOwnProperty(url.hostname)) {
    let exceptions = hostNames[url.hostname];
    for (let i = 0; i < exceptions.length; i++) {
      if ((url.pathname + url.query).toLowerCase().indexOf(exceptions[i]) >= 0) {
        return;
      }
    }
    return {
      redirectUrl
    };
  }
}

browser.runtime.onMessage.addListener(onMessage);

browser.webRequest.onBeforeRequest.addListener(redirect, FILTER, ["blocking"]);
