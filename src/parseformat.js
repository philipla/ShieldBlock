function parseFormat(str) {
  var list = [];
  var spaces = str.split(' ');
  if (spaces.length < 1) return null;
  list[0] = spaces[0];
  if (spaces.length > 1) {
    if (spaces[1].indexOf('except:') >= 0) {
      var commas = spaces[1].split(':')[1].split(',');
      if (commas.length < 1) return null;
      return list.concat(commas);
    } else {
      return null;
    }
  } else {
    return list;
  }
}

module.exports = parseFormat;
