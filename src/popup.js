import { h, app } from 'hyperapp';
import debounce from 'lodash.debounce';
import parseFormat from './parseformat';

function saveState(todos) {
  let hostNames = {};
  for (var i = 0; i < todos.length; i++) {
    const todo = todos[i];
    if (!todo.unchecked) {
      const result = parseFormat(todo.value);
      if (result) {
        hostNames[result[0]] = result.slice(1);
      }
    }
  }

  browser.runtime.sendMessage({ type: 'hostnames', value: hostNames });

  browser.storage.local.set({ shieldblockhostnames: hostNames });
  browser.storage.local.set({ shieldblocktodos: todos });
}

let state = {
  todos: [],
  input: "",
  error: "",
  redirect: "https://nghttp2.org/httpbin/robots.txt"
};

const actions = {
  add: () => state => {
    if (state.input == '') return;

    const result = parseFormat(state.input);
    const todos = state.todos.concat({ value: state.input, unchecked: false });

    if (result) {
      saveState(todos);
      return {
        input: "",
        error: "",
        todos
      };
    } else {
      return { error: "invalid format" };
    }
  },
  todoinput: ({ value }) => ({ input: value }),
  redirectinput: ({ value }) => {
    browser.runtime.sendMessage({ type: 'redirect', value: value });
    browser.storage.local.set({ shieldblockredirect: value });

    return ({ redirect: value });
  },
  remove: ({ todos, index }) => {
    todos.splice(index, 1);
    saveState(todos);

    return ({ todos });
  },
  toggle: ({ todos, index }) => {
    var todo = todos[index];
    todo.unchecked = !todo.unchecked;
    saveState(todos);
    return { todos };
  }
};

const view = (state, actions) => (
  <div>
    <h2>ShieldBlock</h2>

    <div>
    <label>Redirect Url:</label>
    <input
      id="redirect-input"
      type="text"
      oninput={debounce(e => actions.redirectinput({ value: e.target.value }), 250)}
      value={state.redirect}
    />
    </div>

    <div>
      <input
        id="todo-input"
        type="text"
        onkeyup={e => (e.keyCode === 13 ? actions.add() : "")}
        oninput={e => actions.todoinput({ value: e.target.value })}
        value={state.input}
        placeholder="format example: www.website.com except:/page1,/page2"
      />
      <button onclick={actions.add}>＋</button>
    </div>

    <p>
      <h2>{state.error}</h2>
      <ul>
        {state.todos
          .map((t, index) => (
            <li>
              <input type="checkbox" checked={!t.unchecked}
                     onchange={e => actions.toggle({ todos: state.todos, index })} />
              {t.value}
              <button
                onclick={() => actions.remove({ todos: state.todos, index })}>
                x
              </button>
            </li>
          ))}
      </ul>
    </p>
  </div>
);

Promise.all([
    browser.storage.local.get('shieldblocktodos'),
    browser.storage.local.get('shieldblockredirect'),
    browser.storage.local.get('shieldblockcurrentsite')
])
.then((result) => {
  let [todos, redirect, currentsite] = result;

  if (Array.isArray(todos.shieldblocktodos)) {
    state.todos = todos.shieldblocktodos;
  }

  if (redirect.shieldblockredirect) {
    state.redirect = redirect.shieldblockredirect;
  }

  console.log(14, currentsite.shieldblockcurrentsite);
  if (currentsite.shieldblockcurrentsite) {
    state.input = currentsite.shieldblockcurrentsite;
  }

  app(state, actions, view, document.getElementById('app'));
});
