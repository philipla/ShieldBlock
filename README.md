# ShieldBlock

Blocks distracting websites with exceptions to particular pages on the site.

## How to build

Run `node scripts/build.js` to transpile the JavaScript files in the src directory.

Use `./scripts/watch.sh` to automatically build files when they are updated.

## LICENSE

[MIT](LICENSE)

